CC = gcc
CFLAGS = -std=c11 -Wall -Wpedantic -Wextra -Werror -Wconversion
RM = rm -f
PRGS = genere-texte genere-mots ac-matrice ac-hachage
LIBDIR = lib/
SRCDIR = src/
LDLIBS = -lrt

all : $(PRGS)

genere-texte: $(LIBDIR)genere-texte.o $(LIBDIR)generateur.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS)

genere-mots: $(LIBDIR)genere-mots.o $(LIBDIR)generateur.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS)
	
ac-matrice: $(LIBDIR)ac-matrice.o $(LIBDIR)matrice.o $(LIBDIR)pre-ac-matrice.o $(LIBDIR)fifo.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS)

ac-hachage: $(LIBDIR)ac-hachage.o $(LIBDIR)hachage.o $(LIBDIR)pre-ac-hachage.o $(LIBDIR)fifo.o $(LIBDIR)list.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS)

$(LIBDIR)generateur.o: $(SRCDIR)generateur.c $(SRCDIR)generateur.h
	$(CC) $(CFLAGS) -c -o $@ $<
	
$(LIBDIR)genere-texte.o: $(SRCDIR)genere-texte.c $(SRCDIR)generateur.h
	$(CC) $(CFLAGS) -c -o $@ $<
	
$(LIBDIR)genere-mots.o: $(SRCDIR)genere-mots.c $(SRCDIR)generateur.h
	$(CC) $(CFLAGS) -c -o $@ $<
	
$(LIBDIR)ac-matrice.o: $(SRCDIR)ac-matrice.c $(SRCDIR)pre-ac-matrice.h
	$(CC) $(CFLAGS) -c -o $@ $<

$(LIBDIR)ac-hachage.o: $(SRCDIR)ac-hachage.c $(SRCDIR)pre-ac-matrice.h
	$(CC) $(CFLAGS) -c -o $@ $<

$(LIBDIR)pre-ac-matrice.o: $(SRCDIR)pre-ac-matrice.c $(SRCDIR)fifo.h $(SRCDIR)pre-ac-matrice.h
	$(CC) $(CFLAGS) -c -o $@ $<
	
$(LIBDIR)pre-ac-hachage.o: $(SRCDIR)pre-ac-hachage.c $(SRCDIR)fifo.h $(SRCDIR)pre-ac-hachage.h
	$(CC) $(CFLAGS) -c -o $@ $<
	
$(LIBDIR)fifo.o: $(SRCDIR)fifo.c $(SRCDIR)fifo.h
	$(CC) $(CFLAGS) -c -o $@ $<
	
$(LIBDIR)matrice.o: $(SRCDIR)matrice.c $(SRCDIR)matrice.h 
	$(CC) $(CFLAGS) -c -o $@ $<

$(LIBDIR)hachage.o: $(SRCDIR)hachage.c $(SRCDIR)hachage.h $(SRCDIR)list.h
	$(CC) $(CFLAGS) -c -o $@ $<
	
$(LIBDIR)list.o: $(SRCDIR)list.c $(SRCDIR)list.h
	$(CC) $(CFLAGS) -c -o $@ $<

clean: 
	$(RM) $(LIBDIR)*.o $(PRGS) res-ac-matrice res-ac-hachage texte3.txt mots3.txt
