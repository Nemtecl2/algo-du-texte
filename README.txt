Ce projet a été réalisé par Martin Blondel, Clément Drouin et Julien Lamy.

Le répertoire de ce projet est ainsi constitué : 
- dossier src contenant les .c et .h
- dossier lib contenant les .o
- la racine contenant les fichiers de texte et de mots générés, le makefile, le script et les exécutables

Il est possible de lancer le script
./script.sh

De base, la racine ne contient aucun fichier généré et aucun exécutable. Après avoir lancé la commande "make", il est alors possible de lancer :

Le générateur de texte
./genere-texte text_size alph_size > texte.txt

Le générateur de mots
./genere-mots nb min_size max_size alph_size > mots.txt

Aho-Corasick avec la méthode matrice de transitions
./ac-matrice texte.txt mots.txt

Aho-Corasick avec la méthode table de hachage
./ac-hachage texte.txt mots.txt

Il est aussi possible de supprimer les fichiers générés par script.sh (texte3.txt, mots3.txt, res-ac-hachage et res-ac-matrice), les exécutables et les .o
