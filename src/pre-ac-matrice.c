#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "pre-ac-matrice.h"
#include "fifo.h"
#include "generateur.h"

Trie pre_ac_matrice(char *words_file_name) {
  FILE *words_file = NULL;
  
  // Ouverture du fichier des mots
  if ((words_file = fopen(words_file_name, "r")) == NULL) {
    perror("fopen");
    return NULL;
  }
  
  int maxNode = 1;
  size_t array_length = ARRAY_SIZE;
  int current_letter = 0;
  int length = 0;
  size_t cur_arr_size = 0;
  size_t cur_max_size = BUFFER_SIZE;
  char **words = malloc(sizeof(char*) * (size_t) array_length);
  char *buffer = malloc(sizeof(char) * cur_max_size);
  if (buffer == NULL) {
    perror("malloc");
    return NULL;
  }

  // Tant qu'on arrive pas à la fin du fichier, on continue
  while ((current_letter != EOF)) {
    // Si la taille maximale du buffer a été atteinte, on réalloue
    if (length == (int)cur_max_size) {
      cur_max_size = cur_max_size * 2; 
      buffer = realloc(buffer, cur_max_size);
      if (buffer == NULL) {
        perror("realloc");
        return NULL;
      }
    }
    // On lit la lettre courante
    current_letter = getc(words_file);
    // Si c'est un saut de ligne, alors il faut écrire le buffer dans
    // le tableau de mots
    if (current_letter == '\n') {
      // Si la taille maximale du tableau a été atteinte, on réalloue
      if (cur_arr_size == array_length) {
        array_length = array_length * 2;
        words = realloc(words, sizeof(char*) * array_length);
        if (words == NULL) {
          perror("realloc");
          return NULL;
        }
      }
      words[cur_arr_size] = buffer;
      
      // On ajoute la taille de la chaîne actuelle pour calculer
      // le nombre maximal de noeuds
      maxNode += (int)strlen(buffer);
      cur_arr_size++;
      cur_max_size = BUFFER_SIZE;
      length = 0;
      // On vide le buffer pour le mot suivant
      buffer = malloc(sizeof(char) * cur_max_size);
      if (buffer == NULL) {
        perror("malloc");
        return NULL;
      }
    } else { 
      // Si ce n'est pas un saut de ligne, on concatène la lettre au mot
      buffer[length] = (char)current_letter; 
      length++;
    }
  }
  // On free le buffer et on ferme le fichier
  free(buffer);
  fclose(words_file);

  // Création du trie
  Trie trie = createTrie(maxNode);
  if (trie == NULL) {
    return NULL;
  }
  
  // Insertion des mots dans le trie
  for (size_t i = 0; i < cur_arr_size; ++i) {
    insertInTrie(trie, (unsigned char*)words[i]);
  }
  
  // Ajout de la boucle sur l'état initial quand 
  // la transion (q0, lettre) n'existe pas 
  for (int i = FIRST_LETTER; i < LAST_LETTER + 1; ++i) {
    if (trie -> transition[0][i] == -1) {
      trie -> transition[0][i] = 0;
    }
  }
  
  // On appelle completer_matrice pour la création des suppléants
  if (completer_matrice(trie) == -1) {
    return NULL;
  }
  
  return trie;
}

int completer_matrice(Trie trie) {
  // Création de la file
  Fifo f = createFifo();
  // Création de la liste des transitions (e, a, p) telles que p != e
  int *l = trie -> transition[0];
  
  // Boucle pour les successeurs directs de l'état 0
  for (int i = 0; i < UCHAR_MAX; ++i) {
    // On agit que sur les transitions e != p
    if (l[i] != 0 && l[i] != -1) {
      enqueue(f, l[i]);
      trie -> substitute[l[i]] = 0;
    }
  }
  
  // Boucle pour le reste des états
  while (f -> first != NULL) {
    int current_node = dequeue(f);
    l = trie -> transition[current_node];
    for (int i = 0; i < UCHAR_MAX; ++i) {
      // On traite seulement quand il existe une transition
      if (l[i] != -1) {
        enqueue(f, l[i]);
        int sup = trie -> substitute[current_node];
        
        while (trie -> transition[sup][i] == -1) {
          sup = trie -> substitute[sup];
        }
        trie -> substitute[l[i]] = trie -> transition[sup][i];
        if (trie -> finite[trie -> transition[sup][i]] != 0) {
          trie->finite[l[i]]++;
        }
      }
    }
  }
  return 0;
}
