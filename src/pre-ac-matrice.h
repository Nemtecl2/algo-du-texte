#include "matrice.h"

#ifndef PRE_AC_MATRICE_H
#define PRE_AC_MATRICE_H

#define BUFFER_SIZE 2048
#define MAX_NODE 6000
#define ARRAY_SIZE 4


/**
 * Fonction permettant de créer le trie depuis le fichier des mots
 * @param words_file_name nom du fichier dans lequel on lit les mots
 * @return un Trie en cas de succès, -1 sinon
 */
Trie pre_ac_matrice(char *words_file_name);

/**
 * Fonction permettant de créer la fonction de suppléance
 * @param trie le Trie
 * @return 0 en cas de succès, -1 sinon
 */
int completer_matrice(Trie trie);

#endif
