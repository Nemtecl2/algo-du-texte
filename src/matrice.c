#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

#include "matrice.h"

Trie createTrie(int maxNode) {
  // Vérification du paramètre
  if (maxNode <= 0) {
    fprintf(stderr, "Veuillez indiquer un maxNode supérieur à 0\n");
    return NULL;
  }
  
  // Première dimension de la matrice
  int **transition = malloc((unsigned int) maxNode * sizeof(int *));
  if (transition == NULL) {
    perror("Problème malloc transition (1)\n");
    return NULL;
  }
  
  // Seconde dimension de la matrice
  for (int i = 0; i < maxNode; ++i) {
    transition[i] = malloc(UCHAR_MAX * sizeof(int));
    if (transition[i] == NULL) {
      perror("Problème malloc transition (2)\n");
      return NULL;
    } 
  }
  
  // On remplit la matrice de valeurs par défaut
  for (int i = 0; i < maxNode; ++i) {
    for (int j = 0; j < UCHAR_MAX; j++) {
      transition[i][j] = -1;
    }
  }
  
  // Création du tableau des états finaux
  int *finite = malloc((unsigned int) maxNode * sizeof(int));
  if (finite == NULL) {
    perror("Problème malloc finite\n");
    return NULL;
  }
  
  // Initialisation de chaque état comme NOT_FINAL
  for (int i = 0; i < maxNode; ++i) {
    finite[i] = 0;
  }
  
  // Création du tableau des suppléants
  int *substitute = malloc((unsigned int) maxNode * sizeof(int));
  if (substitute == NULL) {
    perror("Problème malloc substitute\n");
    return NULL;
  }
  
  // Initialisation du tableau des suppléants
  for (int i = 0; i < maxNode; ++i) {
    substitute[i] = -1;
  }
  
  // Création du Trie
  Trie trie = malloc(sizeof(int) * 2 + sizeof(transition)
      + sizeof(finite) + sizeof(substitute));
                      
  trie -> maxNode = maxNode;
  trie -> nextNode = 1;
  trie -> transition = transition;
  trie -> finite = finite;
  trie -> substitute = substitute;
  
  return trie;
}

void insertInTrie(Trie trie, unsigned char *w) {
  // On vérifie que le trie a été initialisé
  if (trie == NULL) {
    fprintf(stderr, "  Veuillez initialiser le trie\n");
    exit(EXIT_FAILURE);
  }
  
  // On parcourt le mot reçu
  int currentNode = 0;
  size_t len = strlen((const char *) w);
  for (size_t i = 0; i < len; ++i) {
    int ascii = w[i];
    
    // On regarde s'il existe déjà une transition
    if (trie -> transition[currentNode][ascii] == -1) {
      // S'il n'y en a pas, on regarde si on peut créer un nouveau noeud
      if (trie -> nextNode >= trie -> maxNode) {
        fprintf(stderr, "  Nombre max de noeuds (%d) atteint\n", 
            trie -> maxNode);
        exit(EXIT_FAILURE);
      }
      trie -> transition[currentNode][ascii] = trie -> nextNode;
      currentNode = trie -> nextNode;
      trie -> nextNode = trie -> nextNode + 1;
    } else { // La transition existe
      currentNode = trie -> transition[currentNode][ascii];
    }
  }
  
  // On indique que le noeud courant est un état final.
  trie -> finite[currentNode]++;
}

int isInTrie(Trie trie, unsigned char *w) {
  // On vérifie que le trie a été initialisé
  if (trie == NULL) {
    fprintf(stderr, "  Veuillez initialiser le trie\n");
    exit(EXIT_FAILURE);
  }
  
  // On parcourt le mot reçu
  int currentNode = 0;
  size_t len = strlen((const char *) w);
  for (size_t i = 0; i < len; ++i) {
    int ascii = w[i];
    
    // On regarde s'il existe déjà une transition
    if (trie -> transition[currentNode][ascii] == -1) {
      // S'il n'existe pas de transition, on sort 
      // et on retoune NOT_IN_TRIE
      return NOT_IN_TRIE;
    } else {
      // Si elle existe on change currentNode pour le noeud indiqué dans
      // la matrice
      currentNode = trie -> transition[currentNode][ascii];
    }
  }
  
  // On regarde si le dernier noeud est un état final
  if (trie -> finite[currentNode] != 0) {
    return IN_TRIE;
  } else {
    return NOT_IN_TRIE;
  }
}
