#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "list.h"

struct _list *insertInList(struct _list *list, int startNode, 
    int targetNode, unsigned char letter) {
  struct _list *l = malloc(sizeof(struct _list));
  if (l == NULL) {
    perror("Problème lors de la création de la cellule");
    exit(EXIT_FAILURE);
  }
  
  l -> startNode = startNode;
  l -> targetNode = targetNode;
  l -> letter = letter;
  l -> next = list;
    
  return l;
}
