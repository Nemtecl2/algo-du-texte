#ifndef MATRICE_H
#define MATRICE_H

#define NOT_IN_TRIE 0  /* Constante indiquant que le trie 
                            ne reconnaît pas le mot             */
#define IN_TRIE 1      /* Constante indiquant que le trie 
                          reconnaît le mot                      */

typedef struct _trie *Trie;

struct _trie {
  int maxNode;       /* Nombre maximal de noeuds du trie    */
  int nextNode;      /* Indice du prochain noeud disponible */
  int **transition;  /* Matrice de transition               */
  int *finite;       /* Etats terminaux                     */
  int *substitute;   /* Les suppléants                      */
};

/**
 * Fonction permettant de créer un trie
 * @param maxNode le nombre maximum de noeuds
 * @return un trie
 */
Trie createTrie(int maxNode);

/**
 * Fonction permettant d'insérer un mot dans un trie
 * @param trie le trie
 * @param w le mot à inserer dans le trie
 */
void insertInTrie(Trie trie, unsigned char *w);

/**
 * Fonction permettant de tester si un mot est contenu dans un trie
 * @param trie le trie
 * @param w le mot à tester
 * @return 0 si faux, 1 si vrai
 */
int isInTrie(Trie trie, unsigned char *w);

#endif
