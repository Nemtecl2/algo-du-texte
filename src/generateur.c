#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#include "generateur.h"

int genere_texte(int text_length, size_t alph_size) {
	// Test des paramètres envoyés
	if (text_length < 0) {
		fprintf(stderr, "La taille du texte doit être supérieure ou égale " 
      "à 0\n");
		return -1;
	}
	
  if (alph_size <= 0) {
	  fprintf(stderr, "La taille de l'alphabet doit être supérieure " 
      "à 0\n");
		return -1;
	}
	
	// Génération du texte
	for (int i = 0; i < text_length; ++i) {
		// Récupération du caractère au hasard
		int ascii_code = FIRST_LETTER + rand() % (int)alph_size;
		fprintf(stdout, "%c", ascii_code);
	}
	fprintf(stdout, "\n");
	
	return 0;
}

int genere_mots(int nb, size_t min_size, size_t max_size,
  size_t alph_size) {
      
  // Test des paramètres envoyés
  if (nb < 0) {
    fprintf(stderr, "Le nombre de mots à générer doit être supérieur "
      "ou égal à 0\n");
		return -1;
  }
  
  if (min_size <= 0) {
    fprintf(stderr, "Le nombre de mots à générer doit être supérieur "
      "à 0\n");
		return -1;
  }
  
  if (min_size > max_size) {
    fprintf(stderr, "La taille minimale ne peut être plus grande que la "
    "taille maximale\n");
		return -1;
  }
  
    if (alph_size <= 0) {
	  fprintf(stderr, "La taille de l'alphabet doit être supérieure " 
      "à 0\n");
		return -1;
	}
  
  // Génération des mots, on appelle nb fois genere_texte
  for (int i = 0; i < nb; ++i) {
    // On crée un taille entre min_size et max_size
	  int size = (rand() % (int)(max_size - min_size + 1)) + (int)min_size;
	  if (genere_texte(size, alph_size) == -1) {
      exit(EXIT_FAILURE);
    }
  }
 
  return 0;
}
