#include "list.h"

#ifndef HACHAGE_H
#define HACHAGE_H

#define NOT_IN_TRIE 0  /* Constante indiquant que le trie 
                            ne reconnaît pas le mot             */
#define IN_TRIE 1      /* Constante indiquant que le trie 
                          reconnaît le mot                      */

typedef struct _trie *Trie;

struct _trie {
  int maxNode;          /* Nombre maximal de noeuds du trie     */
  int nextNode;         /* Indice du prochain noeud disponible  */
  List *transition;     /* Liste d’adjacences                   */
  int *finite;          /* Etats terminaux                      */
  int *substitute;      /* Les suppléants                       */
};

/**
 * Fonction permettant de créer un trie
 * @param maxNode le nombre maximal de noeuds
 * @return un trie
 */
Trie createTrie(int maxNode);

/**
 * Fonction permettant d'insérer un mot dans un trie
 * @param trie le trie
 * @param w le mot à inserer dans le trie
 */
void insertInTrie(Trie trie, unsigned char *w);

/**
 * Fonction permettant de tester si un mot est contenu dans un trie
 * @param trie le trie
 * @param w le mot à tester
 * @return 0 si faux, 1 si vrai
 */
int isInTrie(Trie trie, unsigned char *w);

/**
 * Fonction qui retourne la taille de la table de hachage
 * @param maxNode le nombre maximal de noeuds dans la table de hachage
 * @return la longueur de la table de hachage
 */
int hashlen(int maxNode);

/**
 * Fonction de hachage
 * @param letter l'étiquette de la transition
 * @param startNode le noeud de départ de la transition
 * @param maxNode le nombre de noeuds de la table de hachage
 * @return l'indice de la case de la table de hachage associée à cet 
 *         élément
 */
int hashfun(unsigned char letter, int startNode, int maxNode);

/**
 * Fonction permettant de rechercher une transition dans la table de
 * hachage
 * @param transition la table de hachage
 * @param letter l'étiquette de la transition
 * @param startNode le noeud de départ de la transition
 * @param maxNode le nombre de noeuds de la table de hachage
 * @return targetNode si la transition existe
 *         -1 s'il n'existe pas de transition
 */
int searchInHashTable(List *transition, unsigned char letter,
    int startNode, int maxNode);

#endif
