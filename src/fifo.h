#ifndef FIFO_H
#define FIFO_H

// Structure représentant une cellule
typedef struct _cell *Cell;
struct _cell {
  int node;            /* Le noeud                       */
  struct _cell *next;  /* La cellule suivante            */
};

// Structure représentant la file
typedef struct _fifo *Fifo;
struct _fifo {
  Cell first; /* La première cellule de la file */
};


/**
 * Fonction permettant de créer une file
 * @return une file
 */
Fifo createFifo();

/**
 * Fonction permettant d'enfiler un élément dans la file
 * @param fifo la file
 * @param node l'élément à enfiler
 * @return 0 en cas de succès, -1 si l'opération s'est mal déroulée
 */
int enqueue(Fifo fifo, int node);

/**
 * Fonction permettant de défiler le premier élément de la file
 * @param fifo la file
 * @return le premier élément de la file, -1 si l'opération s'est mal
 * déroulée
 */
int dequeue(Fifo fifo);

#endif
