#ifndef GENERE_TEXTE_H
#define GENERE_TEXTE_H

#define FIRST_LETTER 48
#define LAST_LETTER 118

/**
 * Fonction permettant de générer pseudo-aléatoirement un texte 
 * sur la sortie standard.
 * @param text_length la longueur du texte
 * @param alph_size la taille de l'alphabet
 * @return 0 en cas de succès, -1 sinon
 */
int genere_texte(int text_length, size_t alph_size);

/**
 * Fonction permettant de générer pseudo-aléatoirement des mots
 * sur la sortie standard.
 * @param nb le nombre de mots à générer
 * @param min_size la taille minimale de chaque mot
 * @param max_size la taille maximale de chaque mot
 * @param alph_size la taille de l'alphabet
 * @return 0 en cas de succès, -1 sinon
 */
int genere_mots(int nb, size_t min_size, size_t max_size, size_t alph_size);

#endif
