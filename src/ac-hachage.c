#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "pre-ac-hachage.h"

int main(int argc, char *argv[]) {
  // Vérification des paramètres
	if (argc != 3) {
		printf("USAGE : %s mots.txt text.txt\n", argv[0]);
		return EXIT_FAILURE;
	}
	
  // On vérifie que les fichiers existent
  if (access(argv[1], F_OK) == -1 ) {
    printf("Le fichier %s n'existe pas\n", argv[1]);
    return EXIT_FAILURE;
  }
  
  if (access(argv[2], F_OK) == -1 ) {
    printf("Le fichier %s n'existe pas\n", argv[2]);
    return EXIT_FAILURE;
  }
  
  // Appel à pre_ac_matrice
  Trie trie = pre_ac_hachage(argv[1]);
  if (trie == NULL) {
    return EXIT_FAILURE;
  }
  
  FILE *text_file = NULL;
  
  // On ouvre le fichier du texte
  if ((text_file = fopen(argv[2], "r")) == NULL) {
    perror("fopen");
    return -1;
  }
  
  int current_letter = 0;
  int current_node = 0;
  int nb_occ = 0;
  // On continue tant qu'on ne rencontre pas une fin de ligne ou la fin
  // du fichier
  while ((current_letter != EOF) && (current_letter != '\n')) {
    current_letter = getc(text_file);
    if (current_letter != '\n') {
      // Tant qu'il n'existe pas de transition, on prend le suppléant
      while (searchInHashTable(trie -> transition, 
        (unsigned char) current_letter, 
        current_node, trie -> maxNode) == -1) {
        current_node = trie -> substitute[current_node];
      }
      // Le noeud courant est le suppléant
      current_node = searchInHashTable(trie -> transition, 
        (unsigned char) current_letter, current_node, trie -> maxNode);
      // Si le noeud est final on incrémente le nombre d'occurences
      if (trie -> finite[current_node] != 0) {
        nb_occ += trie -> finite[current_node];
      }
    }
  }
  fclose(text_file);
  
  // On affiche sur la sortie standard le nombre d'occurences
  printf("Nombre d'occurrences : %d\n", nb_occ);
  
  return EXIT_SUCCESS;
}
