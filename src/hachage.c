#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

#include "hachage.h"
#include "list.h"

Trie createTrie(int maxNode) {
  // Vérification du paramètre
  if (maxNode <= 0) {
    fprintf(stderr, "Veuillez indiquez un maxNode supérieur à 0\n");
    return NULL;
  }
  
  // Création de la table de hachage
  List *transition = malloc(sizeof(List *) 
      * (unsigned int) hashlen(maxNode));
  if (transition == NULL) {
    perror("Problème malloc transition\n");
    return NULL;
  }
  
  for (int i = 0; i < hashlen(maxNode); ++i) {
    transition[i] = NULL;
  }
  
  // Création du tableau des états finaux
  int *finite = malloc(sizeof(int) * (unsigned int) maxNode);
  if (finite == NULL) {
    perror("Problème malloc finite");
    return NULL;
  }
  
  // Initialisation de chaque état comme NOT_FINAL
  for (int i = 0; i < maxNode; ++i) {
    finite[i] = 0;
  }
  
  // Création du tableau des suppléants
  int *substitute = malloc((unsigned int) maxNode * sizeof(int));
  if (substitute == NULL) {
    perror("Problème malloc substitute\n");
    return NULL;
  }
  
  // Initialisation du tableau des suppléants
  for (int i = 0; i < maxNode; ++i) {
    substitute[i] = -1;
  }
  
  // Création du Trie
  Trie trie = malloc(sizeof(int) * 2 + sizeof(transition)
      + sizeof(finite) + sizeof(substitute));
      
  trie -> maxNode = maxNode;
  trie -> nextNode = 1;
  trie -> transition = transition;
  trie -> finite = finite;
  trie -> substitute = substitute;
  
  return trie;
}

void insertInTrie(Trie trie, unsigned char *w) {
  // On vérifie que le trie a été initialisé
  if (trie == NULL) {
    perror("  Veuillez initialiser le trie\n");
    exit(EXIT_FAILURE);
  }
  
  // On parcourt le mot reçu
  int currentNode = 0;
  size_t len = strlen((const char *) w);
  for (size_t i = 0; i < len; ++i) {
    int targetNode = searchInHashTable(trie -> transition, w[i], 
      currentNode, trie -> maxNode);
    // On regarde s'il existe déjà une transition
    if (targetNode == -1) {
      // S'il n'y en a pas, on regarde si on peut créer un nouveau noeud
      if (trie -> nextNode >= trie -> maxNode) {
        fprintf(stderr, "  Nombre max de noeud (%d) atteint\n", 
		    trie -> maxNode);
        exit(EXIT_FAILURE);
      }
      int index = hashfun(w[i], currentNode, trie -> maxNode);
      trie -> transition[index] = 
          insertInList(trie -> transition[index], currentNode,
          trie -> nextNode, w[i]);
      currentNode = trie -> nextNode;
      trie -> nextNode = trie -> nextNode + 1;
    } else { // La transition existe
      currentNode = targetNode;
    }
  }
  // On indique que le noeud courant est un état final
  trie -> finite[currentNode] = 1;
}

int isInTrie(Trie trie, unsigned char *w) {
  // On vérifie que le trie a été initialisé
  if (trie == NULL) {
    perror("  Veuillez initialiser le trie\n");
    exit(EXIT_FAILURE);
  }
  
  // On parcourt le mot reçu
  int currentNode = 0;
  size_t len = strlen((const char *) w);
  for (size_t i = 0; i < len; ++i) {
    int targetNode = searchInHashTable(trie -> transition, w[i],
        currentNode, trie -> maxNode);
    
    // On regarde s'il existe déjà une transition
    if (targetNode == -1) {
      // S'il n'existe pas de transition, on sort 
      // et on retoune NOT_IN_TRIE
      return NOT_IN_TRIE;
    } else {
      // Si elle existe on change currentNode
      currentNode = targetNode;
    }
  }
  
  // On regarde si le dernier noeud est un état final
  if (trie -> finite[currentNode] == 1) {
    return IN_TRIE;
  } else {
    return NOT_IN_TRIE;
  }
}

int hashlen(int maxNode) {
  return (maxNode * 4) / 3;
}

int hashfun(unsigned char letter, int startNode, int maxNode) {
  int value = ((startNode + 6067) + letter) * 1277;
  return value % hashlen(maxNode);
}

int searchInHashTable(List *transition, unsigned char letter,
    int startNode, int maxNode) {
    int index = hashfun(letter, startNode, maxNode);
    
    // On recherche si l'élément existe dans la liste à l'indice donné
    // par la fonction de hachage
    List list = transition[index];
    while (list != NULL) {
      // Il existe si seulement on trouve la même lettre et le même
      // noeud de départ
      if (list -> letter == letter && list -> startNode == startNode) {
        return list -> targetNode;
      }
      // A chaque itération, on prend la liste suivante
      list = list -> next;
    }
    // Si on a rien trouvé, on renvoie -1
    return -1;
}


