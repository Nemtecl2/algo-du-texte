#ifndef LIST_H
#define LIST_H

typedef struct _list *List;

struct _list {
  int startNode,        /* Etat de départ de la transition      */
      targetNode;       /* Cible de la transition               */
  unsigned char letter; /* Etiquette de la transition           */
  struct _list *next;   /* Maillon suivant                      */
};

/**
 * Fonction permettant d'insérer une nouvelle cellule en tête de liste
 * @param list la liste dans laquelle on ajoute un élément
 * @param startNode le noeud de départ du nouvel élément
 * @param targetNode le noeud d'arrivée du nouvel élément
 * @param letter l'étiquette de la transition du nouvel élément
 * @return une liste contenant le nouvel élément
 */
struct _list *insertInList(struct _list *list, int startNode, 
    int targetNode, unsigned char letter);


#endif
