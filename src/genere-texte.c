#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#include "generateur.h"

int main(int argc, char *argv[]) {
  // On vérifie le nombre de paramètres
	if (argc != 3) {
		printf("USAGE : %s [text_size] [alph_size]\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	
  // Conversion des paramètres
  size_t alph_size = 0;
  if (sscanf(argv[2], "%zu", &alph_size) == EOF) {
    perror("sscanf");
    exit(EXIT_FAILURE);
  }
  
  // Appel à genere_text
  if (genere_texte(atoi(argv[1]), alph_size) == -1) {
    exit(EXIT_FAILURE);
  }
  
  srand((unsigned)time(NULL));
	
  exit(EXIT_SUCCESS);
}
