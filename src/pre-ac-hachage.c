#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "pre-ac-hachage.h"
#include "fifo.h"
#include "generateur.h"

Trie pre_ac_hachage(char *words_file_name) {
  FILE *words_file = NULL;
  
  // Ouverture du fichier de mots
  if ((words_file = fopen(words_file_name, "r")) == NULL) {
    perror("fopen");
    return NULL;
  }
  
  int maxNode = 1;
  size_t array_length = ARRAY_SIZE;
  int current_letter = 0;
  int length = 0;
  size_t cur_arr_size = 0;
  size_t cur_max_size = BUFFER_SIZE;
  char **words = malloc(sizeof(char*) * (size_t) array_length);
  char *buffer = malloc(sizeof(char) * cur_max_size);
  if (buffer == NULL) {
    perror("malloc");
    return NULL;
  }

  // Tant qu'on arrive pas à la fin du fichier, on continue
  while ((current_letter != EOF)) {
    // Si la taille maximale du buffer a été atteinte, on réalloue
    if (length == (int)cur_max_size) {
      cur_max_size = cur_max_size * 2; 
      buffer = realloc(buffer, cur_max_size);
      if (buffer == NULL) {
        perror("realloc");
        return NULL;
      }
    }
    // On lit la lettre courante
    current_letter = getc(words_file);
    // Si c'est un saut de ligne, alors il faut écrire le buffer dans
    // le tableau de mots
    if (current_letter == '\n') {
      // Si la taille maximale du tableau a été atteinte, on réalloue
      if (cur_arr_size == array_length) {
        array_length = array_length * 2;
        words = realloc(words, sizeof(char*) * array_length);
        if (words == NULL) {
          perror("realloc");
          return NULL;
        }
      }
      
      // On ajoute la taille de la chaîne actuelle pour calculer
      // le nombre maximal de noeuds
      words[cur_arr_size] = buffer;
      
      // On ajoute la taille de la chaîne actuelle pour calculer
      // le nombre maximal de noeuds
      maxNode += (int)strlen(buffer);
      cur_arr_size++;
      cur_max_size = BUFFER_SIZE;
      length = 0;
      // On vide le buffer pour le mot suivant
      buffer = malloc(sizeof(char) * cur_max_size);
      if (buffer == NULL) {
        perror("malloc");
        return NULL;
      }
    } else {
      // Si ce n'est pas un saut de ligne, on concatène la lettre au mot
      buffer[length] = (char)current_letter; 
      length++;
    }
  }
  // On free le buffer et on ferme le fichier
  free(buffer);
  fclose(words_file);

  // Création du trie
  Trie trie = createTrie(maxNode);
  if (trie == NULL) {
    return NULL;
  }

  // Insertion des mots dans le trie
  for (size_t i = 0; i < cur_arr_size; ++i) { 
    insertInTrie(trie, (unsigned char*)words[i]);
  }
  
  // Ajout de la boucle sur l'état initial quand 
  // la transition (q0, lettre) n'existe pas 
  for (int i = FIRST_LETTER; i < LAST_LETTER + 1; ++i) {
    int target_node = searchInHashTable(trie -> transition,
      (unsigned char) i, 0 ,trie -> maxNode);
    if (target_node == -1) {
      int hash = hashfun((unsigned char) i, 0, trie -> maxNode);
      trie -> transition[hash] = insertInList(trie -> transition[hash], 
        0, 0, (unsigned char) i);
    }
  }
  
  // On appelle completer_matrice pour la création des suppléants
  if (completer_hachage(trie) == -1) {
    return NULL;
  }
  
  return trie;
}

int completer_hachage(Trie trie) {
  // Création de la file
  Fifo f = createFifo();
  
  // Création de la liste des transitions (e, a, p) telles que p != e
  // Boucle pour les successeurs directs de l'état 0
  for (int i = 0; i < UCHAR_MAX; ++i) {
    int target_node = searchInHashTable(trie -> transition, 
      (unsigned char) i, 0, trie -> maxNode);
    // On agit que sur les transition e != p
    if (target_node != 0 && target_node != -1) {
      enqueue(f, target_node);
      trie -> substitute[target_node] = 0;
    }
  }
  
  // Boucle pour le reste des états
  while (f -> first != NULL) {
    int current_node = dequeue(f);
    for (int i = 0; i < UCHAR_MAX; ++i) {
      int target_node = searchInHashTable(trie -> transition, 
        (unsigned char) i, current_node, trie -> maxNode);
      // On traitre seulement quand il existe une transition
      if (target_node != -1) {
        enqueue(f, target_node);
        int sup = trie -> substitute[current_node];
        while (searchInHashTable(trie -> transition, (unsigned char) i,
            sup, trie -> maxNode) == -1) {
          sup = trie -> substitute[sup];
        }
        int node = searchInHashTable(trie -> transition, 
          (unsigned char) i, sup, trie -> maxNode);
        trie -> substitute[target_node] = node;
        if (trie -> finite[node] != 0) {
          trie->finite[target_node]++;
        }
      }
    }
  }
  return 0;
}
