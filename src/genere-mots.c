#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#include "generateur.h"

int main(int argc, char *argv[]) {
  // On vérifie le nombre de paramètres
	if (argc != 5) {
		printf("USAGE : %s [nb] [min_size] [max_size] [alph_size]\n", argv[0]);
		exit(EXIT_FAILURE);
	}
  
  size_t min_size = 0;
  size_t max_size = 0;
  size_t alph_size = 0;
  
  // Conversion des paramètres
  if (sscanf(argv[2], "%zu", &min_size) == EOF) {
    perror("sscanf");
    exit(EXIT_FAILURE);
  }
  
  if (sscanf(argv[3], "%zu", &max_size) == EOF) {
    perror("sscanf");
    exit(EXIT_FAILURE);
  }
  
  if (sscanf(argv[4], "%zu", &alph_size) == EOF) {
    perror("sscanf");
    exit(EXIT_FAILURE);
  }
  
  srand((unsigned)time(NULL));
  
  // Appel à genere_mots
  if (genere_mots(atoi(argv[1]), min_size, max_size, alph_size) == -1) {
    exit(EXIT_FAILURE);
  }
	
  exit(EXIT_SUCCESS);
}
