#include <stdio.h>
#include <stdlib.h>

#include "fifo.h"

Fifo createFifo() {
  Fifo fifo = malloc(sizeof(fifo));
  if (fifo == NULL) {
    perror("Problème malloc fifo\n");
    return NULL;
  }
  
  fifo -> first = NULL;
  
  return fifo;
}

int enqueue(Fifo fifo, int node) {
  // On vérifie que la file est bien initialisée
  if (fifo == NULL) {
    fprintf(stderr, "fifo est NULL\n");
    return -1;
  }
  
  Cell new_cell = malloc(sizeof(new_cell));
  if (new_cell == NULL) {
    perror("Problème malloc new_cell\n");
    return -1;
  }
  
  new_cell -> node = node;
  new_cell -> next = NULL;
  
  // Si le premier élément de la file n'est pas null,
  // on l'insère à la fin
  if (fifo -> first != NULL) {
    Cell last_cell = fifo -> first;
    while (last_cell -> next != NULL) {
        last_cell = last_cell -> next;
    }
    last_cell -> next = new_cell;
  } else {
    fifo -> first = new_cell;
  }
  
  return 0;
}

int dequeue(Fifo fifo) {
  // On vérifie que la file est bien initialisée
  if (fifo == NULL) {
    fprintf(stderr, "fifo est NULL\n");
    return -1;
  }
  
  // Récupération de l'élément en tête de liste
  int node = 0;
  if (fifo -> first != NULL) {
    Cell cell = fifo -> first;
    
    node = cell->node;
    // Le second élément devient donc le premier
    fifo -> first = cell -> next;
    free(cell);
  }
  
  return node;
}
